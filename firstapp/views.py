from django.shortcuts import render
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.models import User
from .forms import AuthUserForm, RegisterUserForm
from addmem.models import *
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.http import HttpResponseNotFound
from django.db.models import Q


class MyprojectLoginView(LoginView):
    template_name = 'index.html'
    form_class = AuthUserForm
    success_url = reverse_lazy('edit_page')

    def get_success_url(self):
        return self.success_url


class RegisterUserView(CreateView):
    model = User
    template_name = 'regist.html'
    form_class = RegisterUserForm
    success_url = reverse_lazy('edit_page')
    success_msg = 'Пользователь успешно создан'

    def form_valid(self, form):
        form_valid = super().form_valid(form)
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        aut_user = authenticate(username=username, password=password)
        login(self.request, aut_user)
        return form_valid


class MyProjectLogout(LogoutView):
    next_page = reverse_lazy('edit_page')


def main_page(request):
    memes = Image.objects.all()
    return render(request, 'main_mem_page.html', {'memes': memes})

def edit(request, id):
    try:
        memes = Image.objects.get(id=id)

        if request.method == "POST":
            memes.name = request.POST.get("name")
            memes.tags = request.POST.get("tags")
            memes.save()
            return HttpResponseRedirect("/")
        else:
            return render(request, "redact.html", {"memes": memes})
    except Image.DoesNotExist:
        return HttpResponseNotFound("<h2>Person not found</h2>")


def delete(request, id):
    try:
        memes = Image.objects.get(id=id)
        memes.delete()
        return HttpResponseRedirect("/")
    except memes.DoesNotExist:
        return HttpResponseNotFound("<h2>Person not found</h2>")


def search_results(request):
    query = request.GET.get('q')
    object_list = Image.objects.filter(
        Q(name__icontains=query) | Q(tags__icontains=query)
    )
    return render(request, 'search.html', {'object_list': object_list})
