from django.urls import path
from firstapp import views

urlpatterns = [
    path('login', views.MyprojectLoginView.as_view(), name='index'),
    path('register', views.RegisterUserView.as_view(), name='nickname'),
    path('', views.main_page, name='main_page'),
    path('logout', views.MyProjectLogout.as_view(), name='logout_page'),
    path('edit/<int:id>/', views.edit),
    path('delete/<int:id>/', views.delete),
    path('search/', views.search_results, name='search_results')
]