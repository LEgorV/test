from django.shortcuts import render
from .models import *
from .forms import *


def upload(request):
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            img_obj = form.instance
            return render(request, 'add_mem_page.html', {'form': form, 'img_obj': img_obj})
    else:
        form = ImageForm()
    return render(request, 'add_mem_page.html', {'form': form})
