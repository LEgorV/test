from .models import *
from django.forms import ModelForm, TextInput


class ImageForm(ModelForm):
    class Meta:
        model = Image
        fields = ('image', 'name', 'tags')
