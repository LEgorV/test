from django.db import models


class Image(models.Model):
    name = models.CharField(max_length=500)
    image = models.ImageField(upload_to='images')
    tags = models.CharField(max_length=500)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Мем'
        verbose_name_plural = 'Мемы'
